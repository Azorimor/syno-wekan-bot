package main

import (
	"internal/bot"
	"internal/database"
	"internal/initializer"
)

func main() {
	initializer.SetupLogger()
	initializer.LoadEnvironment()
	initializer.LoadConfigFromEnv()
	database.ConnectDB()
	database.Migrate()
	bot.Start()
}
