package initializer

import (
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
)

func LoadEnvironment() {
	log.Info("Loading ENV...")
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}
