package initializer

import (
	"os"
	"strconv"

	log "github.com/sirupsen/logrus"
)

type Config struct {
	DB_URL         string
	PORT           int
	WEKAN_TOKEN    string
	WEKAN_URL      string
	SYNOLOGY_TOKEN string
	SYNOLOGY_URL   string
}

var CONFIG Config

func LoadConfigFromEnv() {

	port, err := strconv.Atoi(os.Getenv("PORT"))
	if err != nil {
		log.WithFields(log.Fields{
			"PORT":       os.Getenv("PORT"),
			"parsedPort": port,
			"error":      err,
		}).Fatal("Cannot load application port from the environment.")
	}

	CONFIG = Config{
		DB_URL:         os.Getenv("DB_URL"),
		PORT:           port,
		WEKAN_TOKEN:    os.Getenv("WEKAN_TOKEN"),
		WEKAN_URL:      os.Getenv("WEKAN_URL"),
		SYNOLOGY_TOKEN: os.Getenv("SYNOLOGY_TOKEN"),
		SYNOLOGY_URL:   os.Getenv("SYNOLOGY_URL"),
	}
}
