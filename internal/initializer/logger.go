package initializer

import (
	log "github.com/sirupsen/logrus"
)

func SetupLogger() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.DebugLevel)
	log.Info("Logger successfully initialized.")
}
