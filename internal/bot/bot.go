package bot

import (
	"internal/handler/synology"
	"internal/handler/wekan"
	"internal/initializer"
	"strconv"

	log "github.com/sirupsen/logrus"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/helmet"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

func Start() {
	log.Println("starting bot...")

	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello World!")
	})

	setupDefaultMiddlewares(app)
	setupRoutes(app)

	port := ":" + strconv.Itoa(initializer.CONFIG.PORT)
	log.Fatal(app.Listen(port))
}

func setupRoutes(app *fiber.App) {
	log.Info("Setting up Routes.")
	wekanRoutes := app.Group("/wekan", wekan.CheckWekanToken)
	wekanRoutes.Post("/", wekan.Incoming)

	synologyRoutes := app.Group("/synology", synology.CheckSynologyToken)
	synologyRoutes.Post("/", synology.Incoming)
}

func setupDefaultMiddlewares(app *fiber.App) {
	log.Info("Setting up Middleware.")
	app.Use(logger.New())
	app.Use(cors.New())
	app.Use(helmet.New())
}
