package synology

type SynologyEvent struct {
	Actions    []Action `form:"actions,omitempty" json:"actions,omitempty"`
	CallbackID string   `form:"callback_id,omitempty" json:"callback_id,omitempty"`
	PostID     int      `form:"post_id,omitempty" json:"post_id,omitempty"`
	Token      string   `form:"token,omitempty" json:"token,omitempty"`
	User       User     `form:"user,omitempty" json:"user,omitempty"`
	UserID     int      `form:"user_ids,omitempty" json:"user_ids,omitempty"`
	Username   string   `form:"username,omitempty" json:"username,omitempty"`
	Timestamp  uint     `form:"timestamp,omitempty" json:"timestamp,omitempty"`
	Text       string   `form:"text,omitempty" json:"text,omitempty"`
}

type Action struct {
	Name  string `form:"name,omitempty" json:"name,omitempty"`
	Style string `form:"style,omitempty" json:"style,omitempty"`
	Text  string `form:"text,omitempty" json:"text,omitempty"`
	Type  string `form:"type,omitempty" json:"type,omitempty"`
	Value string `form:"value,omitempty" json:"value,omitempty"`
}

type User struct {
	UserID   int    `form:"user_id,omitempty" json:"user_id,omitempty"`
	Username string `form:"username,omitempty" json:"username,omitempty"`
}

type BotMessage struct {
	Token   string `json:"token,omitempty"`
	Text    string `json:"text,omitempty"`
	UserIDs []int  `json:"user_ids,omitempty"`
}
