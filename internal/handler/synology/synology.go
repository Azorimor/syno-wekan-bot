package synology

import (
	"github.com/gofiber/fiber/v2"
	log "github.com/sirupsen/logrus"
)

func Incoming(c *fiber.Ctx) error {
	event := new(SynologyEvent)
	if err := c.BodyParser(event); err != nil {
		return err
	}
	log.Debug(event)
	SendMessage("test", 4)
	return c.SendString("Synology incoming message")
}
