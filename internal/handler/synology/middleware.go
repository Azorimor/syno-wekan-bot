package synology

import (
	"github.com/gofiber/fiber/v2"

	"internal/initializer"
)

type Message struct {
	Token     string `form:"token"`
	UserID    int    `form:"user_id"`
	Username  string `form:"username"`
	PostID    int    `form:"post_id"`
	ThreadID  int    `form:"thread_id"`
	Timestamp int    `form:"timestamp"`
	Text      string `form:"text"`
}

// Compare the "incoming synology token value" header to the configured value
func CheckSynologyToken(c *fiber.Ctx) error {
	msg := new(Message)
	if err := c.BodyParser(msg); err != nil {
		return err
	}

	if msg.Token != initializer.CONFIG.SYNOLOGY_TOKEN {
		return fiber.ErrUnauthorized
	}
	return c.Next()
}
