package synology

import (
	"bytes"
	"encoding/json"
	"internal/initializer"
	"io"
	"net/http"
	"net/url"

	log "github.com/sirupsen/logrus"
)

func SendMessage(msg string, channelID int) {
	botMsg := &BotMessage{
		Text:    msg,
		UserIDs: []int{channelID},
	}
	sendPostRequest(initializer.CONFIG.SYNOLOGY_URL, botMsg)
}

func sendPostRequest(urlStr string, payload *BotMessage) {
	data := url.Values{}
	b, err := json.Marshal(payload)
	if err != nil {
		log.Error("Cannot convert payload to json format.", err)
	}
	data.Set("payload", string(b))

	client := &http.Client{}
	req, err := http.NewRequest("POST", urlStr, bytes.NewBufferString(data.Encode()))
	if err != nil {
		log.Error(err)
		return
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error(err)
	}
	log.Debug(string(body))

	defer resp.Body.Close()

	log.Debug("Synology Response Body:", body)
	// FIXME Maybe check the response Errorcode 800 ... inside body response
}
