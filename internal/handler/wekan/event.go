package wekan

type WekanEvent struct {
	Text        string `json:"text"`
	User        string `json:"user"`
	Description string `json:"description"`
	CardID      string `json:"cardId"`
	ListID      string `json:"listId"`
	BoardID     string `json:"boardId"`
	Card        string `json:"card"`
	SwimlineID  string `json:"swimlaneId"`
}
