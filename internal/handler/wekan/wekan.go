package wekan

import (
	"github.com/gofiber/fiber/v2"
	log "github.com/sirupsen/logrus"
)

func Incoming(c *fiber.Ctx) error {
	event := new(WekanEvent)
	if err := c.BodyParser(event); err != nil {
		return err
	}
	log.Debug(event)
	return c.SendString("Wekan incoming message")
}
