package wekan

import (
	"github.com/gofiber/fiber/v2"

	"internal/initializer"
)

// Compare the "X-wekan-token" header to the configured value
func CheckWekanToken(c *fiber.Ctx) error {
	token := c.Get("X-wekan-token", "")
	if token != initializer.CONFIG.WEKAN_TOKEN {
		return fiber.ErrUnauthorized
	}
	return c.Next()
}
