package database

import (
	model "internal/database/model"
	"internal/initializer"

	log "github.com/sirupsen/logrus"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

func ConnectDB() {
	var err error
	dsn := initializer.CONFIG.DB_URL
	log.WithFields(log.Fields{
		"DB_URL": dsn,
	}).Info("Trying to connect to database.")
	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal("Unable to connect to database.")
	}
}

func Migrate() {
	log.Info("Starting potential database migration.")
	DB.AutoMigrate(&model.User{}, &model.WekanUser{}, &model.SynologyUser{})
	log.Info("Finished database migration.")
}
