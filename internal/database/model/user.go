package model

import "gorm.io/gorm"

type User struct {
	gorm.Model
	WekanUserID    uint
	WekanUser      WekanUser
	SynologyUserID uint
	SynologyUser   SynologyUser
	language       string
	Verified       bool   `json:"verified"`
	ConnectToken   string `json:"connect_token"`
}

type WekanUser struct {
	gorm.Model
	Username    string `json:"username"`
	WekanUserID string `json:"wekan_user_id"`
	Verified    bool   `json:"verified"`
}

type SynologyUser struct {
	gorm.Model
	Username       string `json:"username"`
	SynologyUserID int    `json:"synology_user_id"`
	Verified       bool   `json:"verified"`
}
